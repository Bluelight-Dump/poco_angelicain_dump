## angelicain-user 10 QP1A.190711.020 V12.0.17.0.QCRINXM release-keys
- Manufacturer: xiaomi
- Platform: mt6765
- Codename: angelicain
- Brand: POCO
- Flavor: angelicain-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: V12.0.17.0.QCRINXM
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/angelicain/angelicain:10/QP1A.190711.020/V12.0.17.0.QCRINXM:user/release-keys
- OTA version: 
- Branch: angelicain-user-10-QP1A.190711.020-V12.0.17.0.QCRINXM-release-keys-random-text-24496966625741
- Repo: poco_angelicain_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
