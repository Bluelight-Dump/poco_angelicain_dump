#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:6da0b2c17bb2ef42f37482973c2746cd150a3ca1; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:356e46a7e034a0fd048e39c0272c6ae082d912e9 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:6da0b2c17bb2ef42f37482973c2746cd150a3ca1 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
